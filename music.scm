(declare (usual-integrations))
;;; Intervals ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; how many semitones is note2 above note1?
(define (semitones-between note1 note2)
  (- (note->integer note2)
     (note->integer note1)))

(define (interval-check interval)
  (lambda (note1 note2)
    (if (or (rest? note1) (rest? note2))
        #f
        (let ((diff (modulo (abs (semitones-between note1 note2)) 12)))
          (or (= diff interval)
              (= diff (- 12 interval))))))) ;; allows inversions

;;; Scales ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (make-scale "d e f# g a b c#") -> a 7 note sequence
(define (make-scale scale-string)
  (map (lambda (pitch) (make-note pitch 4 1))
       (string-split scale-string)))

;; Semitones between notes e.g. (2 2 1 2 2 2 1) for a major scale
(define (steps-between melody)
  (let ((melody-ints (map note->integer melody)))
    (map mod12
         (map (lambda (n1 n2) (- n1 n2))
              (append (cdr melody-ints) (list (car melody-ints)))
              melody-ints))))

;; count steps away from the axis
(define (count-steps melody scale)
  (let ((axis (car ((note->degree scale) (car melody)))))
    (map (lambda (note)
           (let ((degree (car ((note->degree scale) note))))
             (+ (- degree axis)
                (* 8 (- (note-octave note) (note-octave (car melody)))))))
         melody)))

(define (in-key? note scale)
  (if (rest? note)
      #t
      (true? (member (mod12 (note->integer note))
                     (map mod12 (map note->integer scale))))))

(define (find-position element l pred)
  (letrec ((loop (lambda (l count)
                   (if (null? l)
                       #f ;No such element found
                       (if (pred (car l) element)
                           count
                           (loop (cdr l) (+ count 1)))))))
    (loop l 0)))

;; (define ((scale-degree scale) note)
;;   (let ((pos (find-position note scale same-class?)))
;;     (if pos
;;         pos
;;         (error "Out-of-key accidental"))))

;;; Transposition ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; adds sharps going up, flats going down
(define (shift-up note semitones)
  (if (rest? note)
      note
      (let ((note-with-sharp (integer->note (+ (note->integer note) semitones)
                                            (note-length note))))
        (if (positive? semitones)
            note-with-sharp
            (if (= (get-accidental note-with-sharp) 0) ;; natural?
                note-with-sharp
                (make-note (string-append (note-pitch (shift-up note-with-sharp 1))
                                          "b")
                           (note-octave note-with-sharp)
                           (note-length note)))))))

(define (chromatic-transpose semitones)
  (lambda (melody)
    (map (lambda (note) (shift-up note semitones)) melody)))
;; (map same-class? d-major ((chromatic-transpose 2) c-major))
;; -> (#t #t #t #t #t #t #t)

(define semitone-up (chromatic-transpose 1))

(define fifth-down (chromatic-transpose -7))

(define fifth-up (chromatic-transpose 7))

;; requires correct spelling
;; recturns (degree accidental octave length) e.g., (5 -1 _ _) for a diminished fifth
(define ((note->degree scale) note)
  (if (rest? note)
      '(0 0 4 ,(note-length note))
      (let ((deg (find-position note scale same-letter?)))
        (list (mod7 (+ deg 1))
              (semitones-between (make-note (note-pitch (list-ref scale deg))
                                            (note-octave note) 1)
                                 note)
              (note-octave note)
              (note-length note)))))

(define ((degree->note scale) degree accidental octave duration)
  (let ((interval (list-ref scale (mod7 (- degree 1)))))
    (make-note (note-pitch (respell (shift-up interval accidental)
                                    (get-letter interval)))
               octave
               duration)))

;; todo: make octaves work for > 1 octave difference
(define ((diatonic-shift steps scale) note)
  (if (rest? note)
      note
      (let* ((old-deg-info ((note->degree scale) note)) 
             (new-degree (mod7 (+ (first old-deg-info) steps)))
             (accidental (second old-deg-info))
             (octave (third old-deg-info))
             (duration (fourth old-deg-info))
             (new-note (apply (degree->note scale)
                              (cons new-degree (cdr old-deg-info))))
             ;; correct octave
             (octave-diff (cond ((and (> steps 0)
                                      (after? note new-note)) +1)
                                ((and (< steps 0)
                                      (after? new-note note)) -1)
                                (else 0))))
        ((degree->note scale) new-degree accidental (+ octave octave-diff) duration))))

;; (shift-up note
;;        (apply +
;;               (if (> steps 0)
;;                   (map (lambda (degree) 
;;                          (list-ref (steps-between scale) degree))
;;                        (map mod7 (iota steps ((scale-degree scale) note))))
;;                   (map (lambda (degree)
;;                          (list-ref (map - (steps-between scale)) degree))
;;                        (map mod7 (iota (abs steps)
;;                                        (- ((scale-degree scale) note) 1)
;;                                        -1))))))))

;; Transposes in the key by the given number of steps. E.g., 2 will transpose up a
;;  third/2 scale steps
(define ((diatonic-transpose steps scale) melody)
  (map (diatonic-shift steps scale) melody))

;;; Inversion ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define ((diatonic-inversion scale) melody)
  (map (lambda (note step)
         ((diatonic-shift (- (* 2 step)) scale) note))
       melody
       (count-steps melody scale)))

;;;; API ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Are the two notes <interval> apart or <inverted interval> apart?
(define second? (interval-check 2))
(define minor-third? (interval-check 3))
(define major-third? (interval-check 4))
(define third? (lambda (n1 n2) (or (major-third? n1 n2) (minor-third? n1 n2))))
(define fourth? (interval-check 5))
(define fifth? (interval-check 7))
(define sixth? (interval-check 9))
(define seventh? (interval-check 11))
(define octave? (interval-check 0))

;; scales can be made using the circle of fifths:
;; sharp keys:
(define c-major (make-scale "c d e f g a b"))
(define g-major (fifth-up c-major))
(define d-major (fifth-up g-major))
(define a-major (fifth-up d-major))
(define e-major (fifth-up a-major))
(define b-major (fifth-up e-major))
;; flat keys:
(define f-major (fifth-down c-major))
(define bb-major (fifth-down f-major))
(define eb-major (fifth-down bb-major))
(define ab-major (fifth-down eb-major))
(define db-major (fifth-down ab-major))

;; and every minor key is the relative minor of a major key:
;; sharps:
(define a-minor ((diatonic-transpose -2 c-major) c-major))
(define e-minor ((diatonic-transpose -2 g-major) g-major))
(define b-minor ((diatonic-transpose -2 d-major) d-major))
(define f-minor ((diatonic-transpose -2 a-major) a-major))
(define c-minor ((diatonic-transpose -2 e-major) e-major))
(define g-minor ((diatonic-transpose -2 b-major) b-major))
;; flats:
(define d-minor ((diatonic-transpose -2 f-major) f-major))
(define g-minor ((diatonic-transpose -2 bb-major) bb-major))
(define c-minor ((diatonic-transpose -2 eb-major) eb-major))
(define f-minor ((diatonic-transpose -2 ab-major) ab-major))
(define bb-minor ((diatonic-transpose -2 db-major) db-major))

#|
(define c#-major (semitone-up c-major))
(define d-major (semitone-up c#-major))
(define d#-major (semitone-up d-major))
(define e-major (semitone-up d#-major))
(define f-major (semitone-up e-major))
(define f#-major (semitone-up f-major))
(define g-major (semitone-up f#-major))
(define g#-major (semitone-up g-major))
(define a-major (semitone-up g#-major))
(define a#-major (semitone-up a-major))
(define b-major (semitone-up a#-major))

(define a-minor ((diatonic-transpose -2 c-major) c-major))
(define a#-minor ((diatonic-transpose -2 c#-major) c#-major))
(define b-minor ((diatonic-transpose -2 d-major) d-major))
(define c-minor ((diatonic-transpose -2 d#-major) d#-major))
(define c#-minor ((diatonic-transpose -2 e-major) e-major))
(define d-minor ((diatonic-transpose -2 f-major) f-major))
(define d#-minor ((diatonic-transpose -2 f#-major) f#-major))
(define e-minor ((diatonic-transpose -2 g-major) g-major))
(define f-minor ((diatonic-transpose -2 g#-major) g#-major))
(define f#-minor ((diatonic-transpose -2 a-major) a-major))
(define g-minor ((diatonic-transpose -2 a#-major) a#-major))
(define g#-minor ((diatonic-transpose -2 b-major) b-major))
|#
