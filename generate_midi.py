import midi
import csv
# Instantiate a MIDI Pattern (contains a list of tracks)
pattern = midi.Pattern()
# AVOID PREMATURE OPTIMIZATION
# track = midi.Track()
# pattern.append(track)

# defines the tick for the end of the song
end_of_song_tick = 0

# csv is generated in pattern:
#   'pitch','start_tick','duration'
with open('out.csv','rb') as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        # Instantiate a MIDI Track (contains a list of MIDI events)
        track = midi.Track()
        # Append the track to the pattern
        pattern.append(track)
        pitch,start_tick,duration = int(row[0]),int(row[1]),int(row[2])
        end_tick = start_tick + duration
        # Instantiating a MIDI note 'ON' event and adding it to the track
        on = midi.NoteOnEvent(tick=start_tick, velocity=120, pitch=pitch)
        track.append(on)
        # Instantiate a MIDI note 'OFF' event and add it to the track
        off = midi.NoteOffEvent(tick=end_tick, velocity=120, pitch=pitch)
        track.append(off)
        end_of_song_tick = max(end_tick, end_of_song_tick)

# Add the end of track event, append it to the track
eot = midi.EndOfTrackEvent(tick=end_of_song_tick)
track.append(eot)
# Print out the pattern
print pattern
# Save the pattern to disk
midi.write_midifile("generated_songs/out.mid", pattern)
