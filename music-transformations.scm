;;; Generate all the transformations that can be applied to a theme (including nothing)
(define (make-transformations scale)
  (map compose-list
       (combine-from-bins (all-transpositions scale)
                          (make-inversions scale)
                          (make-retrograde))))

;;; Supporting functions below

(define identity (lambda (x) x))

(define (all-transpositions scale)
  (list identity
        (diatonic-transpose 1 scale) 
        (diatonic-transpose -1 scale)
        (diatonic-transpose 2 scale) 
        (diatonic-transpose -2 scale)
        (diatonic-transpose 3 scale) 
        (diatonic-transpose -3 scale) 
        (diatonic-transpose 4 scale)
        (diatonic-transpose -4 scale)
        (diatonic-transpose 5 scale) 
        (diatonic-transpose -5 scale)
        (diatonic-transpose 6 scale) 
        (diatonic-transpose -6 scale)
        ))

(define (basic-transpositions scale)
  (list identity
        (diatonic-transpose 2 scale) 
        (diatonic-transpose -2 scale)
        (diatonic-transpose 3 scale) 
        (diatonic-transpose -3 scale) 
        (diatonic-transpose 4 scale)
        (diatonic-transpose -4 scale)
        (diatonic-transpose 5 scale)
        (diatonic-transpose -5 scale)
        ))


(define (make-inversions scale)
  (list identity
        (diatonic-inversion scale)))

(define (make-retrograde)
  (list identity
        reverse))

;; (combine-from-bins '(1 2) '(a b) '(x y))
;;  -> ((1 a x) (1 a y) (1 b x) (1 b y) (2 a x) (2 a y) (2 b x) (2 b y))
(define (combine-from-bins . bins)
  (if (> (length bins) 1)
      (let ((other-combinations (apply combine-from-bins (cdr bins))))
        (append-map (lambda (e)
                      (map (lambda (comb)
                             (cons e comb))
                           other-combinations))
                    (car bins)))
      (map list (car bins)))) ;; base case

(define (compose . fns)
  (if (= (length fns) 1)
      (car fns)
      (lambda (x) ((car fns) ((apply compose (cdr fns)) x)))))

(define (compose-list fns)
  (apply compose fns))
