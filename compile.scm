(define dir "bin/")

(for-each (lambda (file)
            (cf file dir))
          '("util"
            "note"
            "music"
            "music-to-generic"
            "canvas"
            "music-compatibilities"
            "music-transformations"
            "compatibility"
            "greedy-search"
            "theme-delay-pairs-to-csv"))
