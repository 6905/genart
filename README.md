# GENART: Generative Music Player
The generative music library created for this project is able to produce and play a musical composition based on a theme input by a user. Similar to other generative systems, our system utilizes user-determined compatibility rules and theme variations as well as randomization in order to independently select new features for the musical piece. By means of combination and abstraction, this library elegantly composes original arrangements and can certainly be extended to other types of art in the future. 

## Installation Requirements
* `MIT/GNU-Scheme` latest stable release
* `Python 2` (with `virtualenv` recommended)
* `Clojure` and `Leiningen` (optional, if want to use the `cadence` player)

# CSV GENERATING:
## Installing
* clone this repository: `git clone https://gitlab.com/6905/genart`
* move into the cloned repository: `cd genart`
* setup a Python 2 virtual environment: `virtualenv -p python env`
* activate the virtual environment you just created: `source env/bin/activate`
* move into the python-midi directory: `cd python-midi`
* install python-midi: `python setup.py install`

# Defining a theme:
   Example code (can be found in `themes/goldberg.scm`)  
```
    (define soggetto  
      '(("g" 4 1)  
        ("f#" 4 1)
        ("e" 4 1)
        ("d" 4 1)
        ("b" 4 1)
        ("c" 4 1)
        ("d" 3 1)
        ("g" 3 1)))
        
    (define goldberg
        (map (lambda (args) (apply make-note args)) soggetto))
```
# Transformations:
Transformations are used for determining variations of the themes which can be mixed/matched in the canon construction process to produce the canon. 
New tranformation functions are easy to create as well as easy to add to the `make-transformations` function. Example code can be found in `music-transformations.scm`.
```
    ;;; Generate all the transformations that can be applied to a theme (including nothing)
    (define (make-transformations scale)
      (map compose-list
           (combine-from-bins (make-transpositions scale)
                              (make-inversions scale)
                              (make-retrograde))))
    
    ;;; Supporting functions below
    
    (define identity (lambda (x) x))
    
    (define (make-transpositions scale)
      (list identity
            (diatonic-transpose 2 scale)
            (diatonic-transpose -2 scale)
            (diatonic-transpose 4 scale)
            (diatonic-transpose -4 scale)))
    
    (define (make-inversions scale)
      (list identity
            (diatonic-inversion scale)))
    
    (define (make-retrograde)
      (list identity
            reverse))
   ```
Here are some visualizations of transformations starting with a theme like this:

![original theme](https://gitlab.com/6905/genart/raw/master/presentation/goldberg.png)

Transpose:
![transpose](https://gitlab.com/6905/genart/raw/master/presentation/transpose.png)

Inverse:
![inverse](https://gitlab.com/6905/genart/raw/master/presentation/inverse.png)

Retrograde:
![retrograde](https://gitlab.com/6905/genart/raw/master/presentation/retrograde.png)

# Compatibility Scores:
Compatibility scores are used to define the desirability of different structures in a musical canon. They can be easily
defined and added to the `music-compatibility-rules` function to be reflected on the canon creation process. Example code can
be found in `music-compatibilities.scm`.
```
    (define penalty -20)
    
    (define (constrain-note-count count)
      (lambda (notes)
        (if (> (length (filter (lambda (n) (not (rest? n))) notes)) count)
            (* 100 penalty)
            0))) 
            
    ;;; Map set of notes -> numeric value
    (define (music-score notes)
      (let ((results 
             (map (lambda (p)
                    (let ((rule (car p)) (sym (cadr p)))
                      (list rule sym (rule notes))))
                  music-compatibility-rules)))
        ;; (pp (filter (lambda (r)
        ;;               (< (third r) 0))
        ;;             results))
        (sum (map (lambda (r) (third r)) results))))
        
    (define music-compatibility-rules
      `((,(constrain-note-count 4) count)
        (,reward-thirds thirds)
        (,reward-fifths fifths)
        (,punish-seconds seconds)
        (,punish-sevenths sevenths)
        ))

```

# Generating a Canon CSV:
Before being converted to a MIDI file, a canon must be converted to a CSV file, as MIT/GNU-Scheme
does not have an audio input/output library. Code for doing this can be found in `theme-delay-pairs-to-csv.scm`.
Example code for generating a canon and then immediately converting it into a CSV file can be found in `main.scm`.

```
    ;; Takes a theme, creates a canon, and plays that canon.
    (load "load")  
    
    (define theme ryb)
    (define key c-major)
    (define piece-length 100)
    (define random? #f)
    
    (define (main)
      (let ((theme-delay-pairs (greedy-search theme piece-length key random?)))
        (theme-delay-pairs-to-csv theme-delay-pairs)
        theme-delay-pairs))
    
    (main)

```

# Generating a MIDI for the canon:

A playable MIDI file at `generated_songs/out.mid` is generated from the CVS file at `out.csv` by running `python generate_midi.py`.
This file can then be played by any system that can play MIDI.

# Playing MIDI files:
* MIDI playing can be done by any MIDI player, but we specifically use an opensource project called `Cadence` which can 
 be found at `https://github.com/dylangleason/cadence`.
* `Leiningen` should be installed, as well as `Clojure`. 
Follow instructions on the respective websites for the projects for installation. Note that if you are
using MacOSX and have `homebrew` installed, you can easily download Leiningen with `brew install leiningen`.
* `cd cadence`
* `lein run -m composer.core`
* --> File --> Upload File --> upload `out.mid` --> set BPM --> Play
