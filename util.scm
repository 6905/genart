(declare (usual-integrations))
;;;; Utility functions we write or find
(define (true? thing) (not (false? thing)))

(define (sign n)
  (cond ((> n 0) 1)
        ((< n 0) -1)
        ((= n 0) 0)))

(define (make-mod b)
  (lambda (n)
    (modulo n b)))
(define mod12 (make-mod 12))
(define mod7  (make-mod 7))

(define (sum l) (apply + l))

(define (count-with-pred pred)
  (lambda (l)
    (fold-left (lambda (sum e) (if (pred e)
                                   (+ sum 1)
                                   sum))
               0
               l)))

;; programmingpraxis.com/contents/standard-predule
(define (zip . xss) (apply map list xss))

;; inefficient but easy
(define ((min-with-key key) . things)
  (cond ((null? (cdr things)) (car things))
        ((< (key (car things)) (key (apply (min-with-key key) (cdr things))))
         (car things))
        (else (apply (min-with-key key) (cdr things)))))

(define (rotate l)
  (append (cdr l) (list (car l))))

(define (rotate-around e l)
  (let ((i (cadr (assoc e (zip l (iota (length l)))))))
    (append (list-tail l i) (list-head l i))))

;;; FOUND HERE: http://okmij.org/ftp/Scheme/util.html#string-split
;; -- procedure+: string-split STRING
;; -- procedure+: string-split STRING '()
;; -- procedure+: string-split STRING '() MAXSPLIT
;;
;; Returns a list of whitespace delimited words in STRING.
;; If STRING is empty or contains only whitespace, then the empty list
;; is returned. Leading and trailing whitespaces are trimmed.
;; If MAXSPLIT is specified and positive, the resulting list will
;; contain at most MAXSPLIT elements, the last of which is the string
;; remaining after (MAXSPLIT - 1) splits. If MAXSPLIT is specified and
;; non-positive, the empty list is returned. "In time critical
;;                                         applications it behooves you not to split into more fields than you
;;                                         really need."

;; -- procedure+: string-split STRING CHARSET
;; -- procedure+: string-split STRING CHARSET MAXSPLIT

;; Returns a list of words delimited by the characters in CHARSET in
;; STRING. CHARSET is a list of characters that are treated as delimiters.
;; Leading or trailing delimeters are NOT trimmed. That is, the resulting
;; list will have as many initial empty string elements as there are
;; leading delimiters in STRING.

;; If MAXSPLIT is specified and positive, the resulting list will
;; contain at most MAXSPLIT elements, the last of which is the string
;; remaining after (MAXSPLIT - 1) splits. If MAXSPLIT is specified and
;; non-positive, the empty list is returned. "In time critical
;;                                         applications it behooves you not to split into more fields than you
;;                                         really need."

;; This is based on the split function in Python/Perl

;; (string-split " abc d e f  ") ==> ("abc" "d" "e" "f")
;; (string-split " abc d e f  " '() 1) ==> ("abc d e f  ")
;; (string-split " abc d e f  " '() 0) ==> ()
;; (string-split ":abc:d:e::f:" '(#\:)) ==> ("" "abc" "d" "e" "" "f" "")
;; (string-split ":" '(#\:)) ==> ("" "")
;; (string-split "root:x:0:0:Lord" '(#\:) 2) ==> ("root" "x:0:0:Lord")
;; (string-split "/usr/local/bin:/usr/bin:/usr/ucb/bin" '(#\:))
;; ==> ("/usr/local/bin" "/usr/bin" "/usr/ucb/bin")
;; (string-split "/usr/local/bin" '(#\/)) ==> ("" "usr" "local" "bin")

(define (string-split str . rest)
  (define (inc n) (+ 1 n))
                                        ; maxsplit is a positive number
  (define (split-by-whitespace str maxsplit)
    (define (skip-ws i yet-to-split-count)
      (cond
       ((>= i (string-length str)) '())
       ((char-whitespace? (string-ref str i))
        (skip-ws (inc i) yet-to-split-count))
       (else (scan-beg-word (inc i) i yet-to-split-count))))
    (define (scan-beg-word i from yet-to-split-count)
      (cond
       ((zero? yet-to-split-count)
        (cons (substring str from (string-length str)) '()))
       (else (scan-word i from yet-to-split-count))))
    (define (scan-word i from yet-to-split-count)
      (cond
       ((>= i (string-length str))
        (cons (substring str from i) '()))
       ((char-whitespace? (string-ref str i))
        (cons (substring str from i) 
              (skip-ws (inc i) (- yet-to-split-count 1))))
       (else (scan-word (inc i) from yet-to-split-count))))
    (skip-ws 0 (- maxsplit 1)))

                                        ; maxsplit is a positive number
                                        ; str is not empty
  (define (split-by-charset str delimeters maxsplit)
    (define (scan-beg-word from yet-to-split-count)
      (cond
       ((>= from (string-length str)) '(""))
       ((zero? yet-to-split-count)
        (cons (substring str from (string-length str)) '()))
       (else (scan-word from from yet-to-split-count))))
    (define (scan-word i from yet-to-split-count)
      (cond
       ((>= i (string-length str))
        (cons (substring str from i) '()))
       ((memq (string-ref str i) delimeters)
        (cons (substring str from i) 
              (scan-beg-word (inc i) (- yet-to-split-count 1))))
       (else (scan-word (inc i) from yet-to-split-count))))
    (scan-beg-word 0 (- maxsplit 1)))

                                        ; resolver of overloading...
                                        ; if omitted, maxsplit defaults to
                                        ; (inc (string-length str))
  (if (string-null? str) '()
      (if (null? rest) 
          (split-by-whitespace str (inc (string-length str)))
          (let ((charset (car rest))
                (maxsplit
                 (if (pair? (cdr rest)) (cadr rest) (inc (string-length str)))))
            (cond 
             ((not (positive? maxsplit)) '())
             ((null? charset) (split-by-whitespace str maxsplit))
             (else (split-by-charset str charset maxsplit)))))))
