;;;; Converts a list of tuples of themes with their delays into a .csv
;;;; file, which can then be converted into a MIDI file (.mid) for
;;;; playing. The input should be of the form ((<theme>, delay),
;;;; (<theme>, delay), ...). The output will be in 'example.csv' but
;;;; we should change that.


;; Converts a note and a current tick into a .csv file line string
(define (note->csv-string note current-tick)
  (string (+ 12 (note->integer note)) ; pitch in MIDI format (added 60
				      ; to make it sound higher)
	  #\,
	  (* 100 current-tick) ; start tick (mult by 100 for midi formatting)
	  #\,
	  (* 100 (note-length note)))) ; duration (same as for start ticks)

;; Converts a list of notes into a list of .csv file line strings
(define (create-note-string-list notes-list delay)
  (let loop
      ((notes-list notes-list)
       (current-tick delay)
       (string-list '()))
    (cond ((null? notes-list)
	   string-list)
	  (else
	   (loop
	    (cdr notes-list)
	    (+ current-tick (note-length (car notes-list)))
	    (append string-list
		    (list (note->csv-string (car notes-list)
					    current-tick))))))))

;; This does the actual writing to the .csv file
(define (theme-delay-pairs-to-csv theme-delays)
  (define out (open-output-file "out.csv"))
  (define (walk-list theme-delays)
    (if (and (list? theme-delays) (> (length theme-delays) 0))
        (let ((x (car theme-delays))
              (note-delay (cdar theme-delays))
              (y (cdr theme-delays))
              (string-list
               (create-note-string-list (caar theme-delays)
                                        (cadar theme-delays))))
          (map (lambda (m) (display m out) (newline out))
               string-list)
          (walk-list y))
        (display ".csv conversion complete")))
  (walk-list theme-delays)
  (close-output-port out))

