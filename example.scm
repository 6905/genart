This is the Scheme process buffer.
Type C-x C-e to evaluate the expression before point.
Type C-c C-c to abort evaluation.
Type C-h m for more information.

MIT/GNU Scheme running under OS X

Copyright (C) 2014 Massachusetts Institute of Technology
This is free software; see the source for copying conditions. There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Image saved on Wednesday January 4, 2017 at 12:46:56 AM
Release 9.2 || Microcode 15.3 || Runtime 15.7 || SF 4.41 || LIAR/C 4.118
Edwin 3.116

(load "load")

                                        ;Loading "load.scm"...
                                        ;  Loading "util.scm"... done
                                        ;  Loading "note.scm"... done
                                        ;  Loading "music.scm"... done
                                        ;  Loading "music-to-generic.scm"... done
                                        ;  Loading "canvas.scm"... done
                                        ;  Loading "music-compatibilities.scm"... done
                                        ;  Loading "compatibility.scm"... done
                                        ;... done
                                        ;Value: compatibility

                                        ;Loading "themes/row.scm"... done
                                        ;Value: ryb

ryb
                                        ;Value 2: (#[note 3] #[note 4] #[note 5] #[note 6] #[note 7] #[note 8] #[note 9] #[note 10] #[note 11] #[note 12] #[note 13] #[note 14] #[note 15] #[note 16] #[note 17] #[note 18] #[note 19] #[note 20] #[note 21] #[note 22] #[note 23] #[note 24] #[note 25])

(define nryb (normalize ryb))
                                        ;Value: nryb



(define row-canvas (add-theme blank-canvas nryb 0))
                                        ;Value: row-canvas


;; look at the scores of different delays:
(map car (map (lambda (i) (compatibility row-canvas nryb i music-score)) (iota 12)))
                                        ;Value 74: (0 -125 140 135 135 105 165 140 225 245 195 130)
