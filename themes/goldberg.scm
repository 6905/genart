(define soggetto
  '(("g" 4 1)
    ("f#" 4 1)
    ("e" 4 1)
    ("d" 4 1)
    ("b" 4 1)
    ("c" 4 1)
    ("d" 3 1)
    ("g" 3 1)))

(define goldberg
  (map (lambda (args) (apply make-note args)) soggetto))
