(define plainsong-theme
  '(("c" 4 3)
    ("c" 5 3)
    ("g" 4 2)
    ("f" 4 2)

    ("f" 4 2)
    ("e" 4 2)
    ("f" 4 2)
    ("g" 4 4)

    ("c" 4 3)
    ("c" 5 3)
    ("g" 4 2)
    ("f" 4 2)

    ("f" 4 2)
    ("e" 4 2)
    ("f" 4 2)
    ("g" 4 4)

    ("b" 4 2)
    ("c" 4 2)
    ("c" 4 1)
    ("d" 4 2)
    ("c" 4 3)

    ("e" 4 4)
    ("g" 3 2)
    ("a" 3 4)

    ))

(define plainsong
  (map (lambda (args) (apply make-note args)) plainsong-theme))
