(define inventions-notes
  '(("" 4 2)
    ("f" 4 2)
    ("g" 4 2)
    ("a" 4 2)
    ("bb" 4 2)
    ("g" 4 2)
    ("a" 4 2)
    ("f" 4 2)
    ("c" 5 4)
    ("f" 5 4)
    ("e" 5 1)
    ("d" 5 1)
    ("e" 5 2)
    ("f" 5 4)))

(define inventions
  (map (lambda (args) (apply make-note args)) inventions-notes))
