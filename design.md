
Concepts
--------
 - Pattern - Composed of components. Represents something like a melodic line or a geometric shape
 - Component - element of a pattern; represents something like a musical note or a part of an image
 - Compatibility - components have compatabilities between them. Compatibilities are a numeric value, with a greater value corresponding to how well the position of a component fits; for example, a note fitting into a chord

Problems
--------
 - Representation of space: In music it's simple enough to represent the world as a 2D grid of pitch and time. In visual art, it might be more difficult. We could represent space as pixels in an image, but it might be prohibitively expensive to calculate the compatibilities directly (but maybe with some abstraction/good algorithms. I think if we made rules intelligently it would be possible)

Overal Structure
----------------
 - Take a pattern as input
 - Transform pattern as defined by a set of rules, and place it in space
 - Place patterns such that overall compatibility is maximized
 - "Compile" into media (image, sound, etc)

Algorithm Options
-----------------
Our problem space is really large, so we have to think pretty carefully about how we want to explore it. We could try maximizing an overall compatibility function with like dynamic programming or something.

We could also take a greedy approach to pattern placement, which is probably the easiest option.

Kai suggested teaching a neural net and doing reinforcement learning. I'm not sure how that would work and it sounds kind of hard, but maybe cool.