(declare (usual-integrations))
;; Note Record
(define-record-type note
  (make-note pitch octave length)
  note?
  (pitch note-pitch) ;; technically "pitch class" i.e. A-G w/ sharps and flats
  (octave note-octave) ;; integer (possibly negative)
  (length note-length)) ;; integer representing number of beats

(define (rest? note) 
  (string=? (note-pitch note) ""))

(define class->steps-mapping '(("c" 0) ("d" 2) ("e" 4) ("f" 5)
                               ("g" 7) ("a" 9) ("b" 11)))

(define steps->class-mapping
  (map reverse class->steps-mapping))
(define (get-class steps)
  (let ((class (assoc steps steps->class-mapping)))
    (if class
        (cadr class)
        #f)))
(define (get-steps class)
  (assoc class class->steps-mapping))

(define (string-in? sub str)
  (let ((str-list (string->string-list str)))
    (true? (member (if (char? sub)
                       (char->name sub)
                       sub)
                   str-list))))

(define (string->string-list str)
  (map char->name (string->list str)))

(define (flat? char)
  (string-in? char "b-"))

(define (sharp? char)
  (string-in? char "#+"))

(define (count-flats str)
  ((count-with-pred flat?) (string->string-list str)))

(define (count-sharps str)
  ((count-with-pred sharp?) (string->string-list str)))

(define (pitch->steps pitch)
  (if (string=? pitch "")
      0
      (let ((letter (string-downcase (string-head pitch 1)))
            (accidentals (string-tail pitch 1)))
        (+ (cadr (get-steps letter))
           (count-sharps accidentals)
           (- (count-flats accidentals))))))

(define (note->integer note)
  (+ (pitch->steps (note-pitch note))
     (* 12 (note-octave note))))

(define (integer->note int length)
  (let ((octave (if (negative? int)
                    (- (quotient int 12) 1)
                    (quotient int 12)))
        (steps (modulo int 12)))
    (let ((class (get-class steps)))
      (if class
          (make-note class octave length)
          (make-note (string-append
                      (get-class (modulo (- steps 1) 12))
                      "#")
                     octave
                     length)))))

(define (respell note letter)
  (if (> (string-length letter) 1)
      (error "Give a natural letter (e.g. A) to respell to")
      (let* ((diff (- (cadr (get-steps (string-downcase letter)))
                      (pitch->steps (note-pitch note))))
             (nearest-diff ((min-with-key abs) diff
                            (remainder (- diff (* (sign diff) 12)) 12)))
             (accidental (if (< nearest-diff 0) #\# #\b))
             ;;(old-letter (string-downcase (string-head (note-pitch note) 1))))
             )
        (make-note (string-append letter
                                  (make-string (abs nearest-diff) accidental))
                   (cond ((= (abs diff) 12)
                          (- (note-octave note) (sign diff)))
                         ((and (positive? diff)
                               (negative? nearest-diff))
                          (- (note-octave note) 1))
                         ((and (negative? diff)
                               (positive? nearest-diff))
                          (+ (note-octave note) 1))
                         (else
                          (note-octave note)))
                   (note-length note)))))
#|
(pp (respell (make-note "dbbb" 4 2) "b"))
12
0
#[note 42]
(pitch "b")
(octave 3)
(length 2)


(pp (respell (make-note "b#" 4 2) "c"))
-12
0
#[note 43]
(pitch "c")
(octave 5)
(length 2)
|#

;; (same-class? C0 B#4) -> #t
(define (same-class? note1 note2)
  (if (or (rest? note1) (rest? note2))
      (and (rest? note1) (rest? note2))
      (= (mod12 (note->integer note1)) (mod12 (note->integer note2)))))

(define (get-accidental note)
  (let ((accidentals (string-tail (note-pitch note) 1)))
    (- (count-sharps accidentals)
       (count-flats accidentals))))

(define (same-letter? note1 note2)
  (string=? (get-letter note1)
            (get-letter note2)))

(define (get-letter note)
  (string-head (note-pitch note) 1))

;; is n1 after n2 in (c d e f g a b)?
(define (after? n1 n2)
  (let ((letters (map cons (string-split "c d e f g a b") (iota 8))))
    (> (cdr (assoc (get-letter n1) letters))
       (cdr (assoc (get-letter n2) letters)))))
