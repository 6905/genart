(declare (usual-integrations))
(define theme-delays '())

;; returns random int between [i-n, i]
(define ((random-n n) i)
  (+ (max 0 (- i n)) (random n)))

;; Finds max score for list of (score canvas)
;; returns (score canvas)
(define (max-list elm list)
  (if (= 0 (length list))
      elm
      (if (>= (car elm) (car (car list)))
          (max-list elm (cdr list))
          (max-list (car list) (cdr list)))))

;; Checks if list contains element i
;; returns bool
(define (contains? list i)
  (if (= 0 (length list)) #f
      (or (eq? (car list) i) (contains? (cdr list) i))))

;; Adds unique random position to position list
(define (add-pos i pos-list)
  (let ((new-pos ((random-n 16) i)))
    (if (contains? pos-list new-pos)
        (add-pos i pos-list)
        (get-ran-pos-list i (append pos-list (list new-pos))))))

;; returns list of unique positions
(define (get-ran-pos-list n pos-list)
  ;; change value below to change # of pos to check
  (if (>= (length pos-list) (min n 16))
      pos-list
      (add-pos (max n 1) pos-list)))

;; returns (positions)
(define (get-positions theme-delays n random-flag)
  (if random-flag
      (get-ran-pos-list n (list (+ 1 n)))
      (lset-difference = (append (iota n) (list n (+ 1 n))) (map cadr theme-delays))))

;; Finds best position of given variation
;; returns best (score canvas theme-delays)
(define (check-compat canvas theme-delays variation-pair score current-canvas current-pos positions compat-fn)
  (if (= 0 (length positions))
      (list score current-canvas (append theme-delays `((,(cadr variation-pair) ,current-pos))))
      (let ((compat (compatibility canvas (car variation-pair) (car positions) compat-fn)))
        (if (>= score (car compat))
            (check-compat canvas theme-delays variation-pair score current-canvas current-pos (cdr positions) compat-fn)
            (check-compat canvas theme-delays variation-pair (car compat) (cadr compat) (car positions) (cdr positions) compat-fn)))))

;; returns best new canvas and associated theme-delay structure
;; (canvas theme-delays)
(define (greedy-iter canvas theme-delays compat-fn variation-pairs random-flag)
  (let* ((positions (get-positions theme-delays (vector-length canvas) random-flag))
         (placements (map (lambda (v-pair) (check-compat canvas theme-delays v-pair -1000000 canvas 0 positions compat-fn)) variation-pairs)))
    (cdr (max-list (car placements) (cdr placements)))))

;; entry point
;; returns list of (variation delay) pairs
(define (greedy-search theme music-length scale rules random-flag)
  (let ((transformations (make-transformations scale))
        (norm-theme (normalize theme)))
    (let ((norm-variations (map (lambda (t) (t norm-theme)) transformations))
          (variations (map (lambda (t) (t theme)) transformations)))
      (let ((variation-pairs (zip norm-variations variations)))
        (define (greedy-rec canvas theme-delays)
          (if (>= (vector-length canvas) music-length)
              theme-delays
              (apply greedy-rec
                     (greedy-iter canvas theme-delays (music-score rules)
                                  variation-pairs random-flag))))
        (greedy-rec blank-canvas (list))))))
