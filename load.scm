(define dir "bin/") ;; if compiled
;;(define dir "") ;; if not compiled

(for-each (lambda (file)
            (load (string-append dir file)))
          '("util"
            "note"
            "music"
            "music-to-generic"
            "canvas"
            "music-compatibilities"
            "music-transformations"
            "compatibility"
            "greedy-search"
            "theme-delay-pairs-to-csv"))


(for-each (lambda (file)
            (load file))
          '("themes/row"
            "themes/goldberg"
            "themes/plainsong"
            "theme-delay-pairs-to-csv"
            "themes/inventions"
            "themes/little"))
