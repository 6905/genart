;; Takes a theme, creates a canon, and plays that canon.
;;(load "compile")
(load "load")

(define theme inventions)
(define key f-major)
(define piece-length 120)

;; Tweakable parameters:
(define voices 3)
(define rules (compatibility-rules-basic-triads voices))
;;(define rules (compatibility-rules-punish-dissonance voices))
(define random-flag #t)

(define (main)
  (let ((theme-delay-pairs (greedy-search theme piece-length key rules random-flag)))
    (theme-delay-pairs-to-csv theme-delay-pairs)
    theme-delay-pairs))

(main)
