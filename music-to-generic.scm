(declare (usual-integrations))
;;;; Converts music language into more generic theme/component objects

(define (normalize melody)
  (append-map (lambda (note)
                (make-list (note-length note)
                           (make-note (note-pitch note)
                                      (note-octave note)
                                      1)))
              melody))
  
