(declare (usual-integrations))
;;;; Data structure used to keep track of what's been placed where/when

(define blank-canvas #())

(define empty-slot '())

(define (get-slot canvas position)
  (if (< position (vector-length canvas))
      (vector-ref canvas position)
      empty-slot))

(define (add-theme canvas theme position)
  (let ((element-positions (zip (iota (length theme) position)
                                theme)))
    (make-initialized-vector (max (+ position (length theme))
                                  (vector-length canvas))
                             (lambda (pos)
                               (let ((new-element (assoc pos element-positions)))
                                 (if new-element
                                     (cons (cadr new-element)
                                           (get-slot canvas pos))
                                     (get-slot canvas pos)))))))
