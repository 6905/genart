;;; Map set of notes -> numeric value
(define ((music-score rules) notes)
  (let ((results 
         (map (lambda (p)
                (let ((rule (car p)) (sym (cadr p)))
                  (list rule sym (rule notes))))
              rules)))
    ;; (pp (filter (lambda (r)
    ;;               (< (third r) 0))
    ;;             results))
    (sum (map (lambda (r) (third r)) results))))

#|
We want:
- at least 2 thirds
- at least 1 fifth
- no more than 4 notes
- no nasty intervals

I think that's sufficient to reward creating triads (with inversions)
|#

(define penalty -20)

(define (constrain-note-count count)
  (lambda (notes)
    (if (> (length (filter (lambda (n) (not (rest? n))) notes)) count)
        (* 100 penalty)
        0)))

;; currently rewards having more than _count_ intervals
(define (reward-interval interval-test count reward)
  (lambda (notes)
    (let ((pairs (unique-pairs notes)))
      (* (/ ((count-with-pred (lambda (p) (apply interval-test p))) pairs)
            count)
         reward))))
(define reward-thirds (reward-interval third? 2 30))
(define reward-fifths (reward-interval fifth? 1 15))

(define (punish-interval interval-test penalty)
  (lambda (notes)
    (if (any (lambda (p) (apply interval-test p)) (unique-pairs notes))
        penalty
        0)))
(define punish-seconds (punish-interval second? penalty))
(define punish-sevenths (punish-interval seventh? penalty))

(define (compatibility-rules-basic-triads voices)
  `((,(constrain-note-count voices) count)
    (,reward-thirds thirds)
    (,reward-fifths fifths)
    ))

(define (compatibility-rules-punish-dissonance voices)
  `((,(constrain-note-count voices) count)
    (,reward-thirds thirds)
    (,reward-fifths fifths)
    (,punish-seconds seconds)
    (,punish-sevenths sevenths)
    ))


;; do we want a bonus reward for getting a full chord?

(define (unique-pairs l)
  (if (> (length l) 1)
      (append (map (lambda (e)
                     (list (car l) e))
                   (cdr l))
              (unique-pairs (cdr l)))
      '()))
